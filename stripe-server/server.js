const express = require('express');
const cors = require('cors');
const Stripe = require('stripe');
const bp = require('body-parser')
const app = express()
const port = 8081

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))
app.use(cors({ origin: true }));
app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*');
  next();
});
const stripe = Stripe('sk_test_51L7kuiBtSXXbL4rNIxln4nei0fFLTDjIPmp6DxdauuybuBdwuAKXGNZJtf4Y0Ih0sfCw1f3J0WkzZcUdix7jGDr000ciQl5ZJ8');

app.post('/checkout', async (req, res) => {
  let reservationId = req.body.reservationId
  console.log(reservationId)
  // creation d'un produit lors d'un booking
  const product = await stripe.products.create({
    name: req.body.name,
    description : req.body.description || 'nothing'
  })
  let productId = product.id 
  console.log(productId)
  // creation d'un price object lié au produit
  const price = await stripe.prices.create({
    unit_amount: req.body.price*100,
    currency: 'eur',
    product: productId
  })
  let priceId = price.id
  console.log(priceId)
  // creation d'une session checkout avec le price obj
  const session = await stripe.checkout.sessions.create({
    success_url: 'http://35.187.15.66:3001/payment/success',
    cancel_url: 'http://35.187.15.66:3001/payment/failed',
    line_items: [
      {
        price: priceId,
        quantity: req.body.quantity
      },
    ],
    mode: 'payment',
  });
  console.log(session.id)
  // need to save the sessionId and paymentIntent to the DB
  res.send(session);
})

app.post('/cancel', async (req, res) => {
  // refund the paymentIntent
  try {
    const total = await stripe.paymentIntents.retrieve(
      req.body.paymentIntent
    )
    console.log(total.amount)
    const refund = await stripe.refunds.create({
      payment_intent: req.body.paymentIntent,
      amount : total.amount
    })
    res.status(200).send(refund.status)
  } catch (error) {
    res.status(400).send(error.raw.message)
  }
}),

app.post('/refund', async (req, res) => {
  // refund the paymentIntent
  try {
    const refund = await stripe.refunds.create({
      payment_intent: req.body.paymentIntent,
      amount : (req.body.price * 100)
    })
    res.status(200).send(refund.status)
  } catch (error) {
    res.status(400).send(error.raw.message)
  }

})

app.get('/', (req, res) => {
  res.send('Stripe server running!')
})

app.listen(port, () => {
  console.log(`Stripe server listening on port ${port}`)
})
