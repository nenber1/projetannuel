<?php

namespace App\Tests\Unit;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\TimeSlot;
use App\Entity\User;

class ReservationTest extends ApiTestCase
{
    private Reservation $reservation;

    public function setUp(): void
    {
        parent::setUp();

        $this->reservation = new Reservation();
    }

    public function testGetDate(): void
    {
        // GIVEN
        $value = new \DateTime('@'.strtotime('now'));

        // WHEN
        $response = $this->reservation->setCreatedAt($value);

        // THEN
        self::assertInstanceOf(Reservation::class, $response);
        self::assertEquals($value, $this->reservation->getCreatedAt());
    }

    public function testWrongDate(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "date";

        // WHEN
        $response = $this->reservation->setCreatedAt($value);
    }

    public function testGetRoomId(): void
    {
        // GIVEN
        $value = new Room();

        // WHEN
        $response = $this->reservation->setRoomId($value);

        // THEN
        self::assertInstanceOf(Reservation::class, $response);
        self::assertEquals($value, $this->reservation->getRoomId());
    }

    public function testWrongRoomId(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "wrong";

        // WHEN
        $response = $this->reservation->setRoomId($value);
    }

    public function testGetSlotId(): void
    {
        // GIVEN
        $value = new TimeSlot();

        // WHEN
        $response = $this->reservation->setSlotId($value);

        // THEN
        self::assertInstanceOf(Reservation::class, $response);
        self::assertEquals($value, $this->reservation->getSlotId());
    }

    public function testWrongSlotId(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "wrong";

        // WHEN
        $response = $this->reservation->setSlotId($value);
    }

    public function testGetClientId(): void
    {
        // GIVEN
        $value = new User();

        // WHEN
        $response = $this->reservation->setClientId($value);

        // THEN
        self::assertInstanceOf(Reservation::class, $response);
        self::assertEquals($value, $this->reservation->getClientId());
    }

    public function testWrongClientId(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "wrong";

        // WHEN
        $response = $this->reservation->setClientId($value);
    }

    public function testGetQuantity(): void
    {
        // GIVEN
        $value = 3;

        // WHEN
        $response = $this->reservation->setQuantity($value);

        // THEN
        self::assertInstanceOf(Reservation::class, $response);
        self::assertEquals($value, $this->reservation->getQuantity());
    }

    public function testWrongQuantity(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "wrong";

        // WHEN
        $response = $this->reservation->setQuantity($value);
    }

}
