<?php

namespace App\Tests\Unit;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Reservation;
use App\Entity\User;

class UserTest extends ApiTestCase
{
    private User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
    }

    public function testGetEmail(): void
    {
        // GIVEN
        $value = 'test@test.fr';

        // WHEN
        $response = $this->user->setEmail($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getEmail());
        self::assertEquals($value, $this->user->getUsername());
    }

    public function testGetRoles(): void
    {
        // GIVEN
        $value = ["ROLE_USER"];

        // WHEN
        $response = $this->user->setRoles($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getRoles());
    }

    public function testWrongRole(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "wrong";

        // WHEN
        $response = $this->user->setRoles($value);
    }

    public function testGetPassword(): void
    {
        // GIVEN
        $value = 'azertylol';

        // WHEN
        $response = $this->user->setPassword($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPassword());
    }

    public function testGetLastname(): void
    {
        // GIVEN
        $value = 'Mozart';

        // WHEN
        $response = $this->user->setLastName($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getLastName());
    }

    public function testGetFirstname(): void
    {
        // GIVEN
        $value = 'Name';

        // WHEN
        $response = $this->user->setFirstName($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getFirstName());
    }

    public function testGetAdress(): void
    {
        // GIVEN
        $value = 'rue de la paix';

        // WHEN
        $response = $this->user->setAdress($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getAdress());
    }

    public function testGetCity(): void
    {
        // GIVEN
        $value = 'paris';

        // WHEN
        $response = $this->user->setCity($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getCity());
    }

    public function testGetPhone(): void
    {
        // GIVEN
        $value = '0102030405';

        // WHEN
        $response = $this->user->setPhone($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPhone());
    }

    public function testGetBirthday(): void
    {
        // GIVEN
        $value = '01/02/1988';

        // WHEN
        $response = $this->user->setBirthday($value);

        // THEN
        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getBirthday());
    }


    public function testGetReservation(): void
    {
        // GIVEN
        $value = new Reservation();
        $value1 = new Reservation();
        $value2 = new Reservation();

        // WHEN
        $this->user->addReservation($value);
        $this->user->addReservation($value1);
        $this->user->addReservation($value2);

        // THEN
        self::assertCount(3, $this->user->getReservations());
        self::assertTrue($this->user->getReservations()->contains($value));
        self::assertTrue($this->user->getReservations()->contains($value1));
        self::assertTrue($this->user->getReservations()->contains($value2));

        $response = $this->user->removeReservation($value);

        self::assertInstanceOf(User::class, $response);
        self::assertCount(2, $this->user->getReservations());
        self::assertFalse($this->user->getReservations()->contains($value));
        self::assertTrue($this->user->getReservations()->contains($value1));
        self::assertTrue($this->user->getReservations()->contains($value2));

    }

    public function testWrongReservation(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "salut";

        // WHEN
        $this->user->addReservation($value);

    }





}
