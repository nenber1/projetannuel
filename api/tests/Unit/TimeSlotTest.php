<?php

namespace App\Tests\Unit;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\TimeSlot;
use App\Entity\User;
use Symfony\Component\Validator\Constraints\Time;

class TimeSlotTest extends ApiTestCase
{
    private TimeSlot $timeSlot;

    public function setUp(): void
    {
        parent::setUp();

        $this->timeSlot = new TimeSlot();
    }

    public function testGetAvailable(): void
    {
        // GIVEN
        $value = true;

        // WHEN
        $response = $this->timeSlot->setAvailable($value);

        // THEN
        self::assertInstanceOf(TimeSlot::class, $response);
        self::assertEquals($value, $this->timeSlot->GetAvailable());
    }


    public function testSlotStart(): void
    {
        // GIVEN
        $value = new \DateTime('@'.strtotime('now'));


        // WHEN
        $response = $this->timeSlot->setSlotStart($value);

        // THEN
        self::assertInstanceOf(TimeSlot::class, $response);
        self::assertEquals($value, $this->timeSlot->GetSlotStart());
    }

    public function testWrongSlotStart(): void
    {
        // THEN
        $this->expectException(\Error::class);

        // GIVEN
        $value = "salut";

        // WHEN
        $this->timeSlot->setWrongSlotStart($value);

    }

    public function testSlotEnd(): void
    {
        // GIVEN
        $value = new \DateTime('@'.strtotime('now'));

        // WHEN
        $response = $this->timeSlot->setSlotEnd($value);

        // THEN
        self::assertInstanceOf(TimeSlot::class, $response);
        self::assertEquals($value, $this->timeSlot->GetSlotEnd());
    }

    public function testWrongSlotEnd(): void
    {
        // THEN
        $this->expectException(\Error::class);

        // GIVEN
        $value = "salut";

        // WHEN
        $this->timeSlot->setWrongSlotEnd($value);

    }

    public function testRoom(): void
    {
        // GIVEN
        $value = new Room();

        // WHEN
        $response = $this->timeSlot->setRoomId($value);

        // THEN
        self::assertInstanceOf(TimeSlot::class, $response);
        self::assertEquals($value, $this->timeSlot->GetRoomId());
    }

    public function testWrongRoom(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "salut";

        // WHEN
        $this->timeSlot->setRoomId($value);

    }

    public function testGetReservation(): void
    {
        // GIVEN
        $value = new Reservation();

        // WHEN
        $this->timeSlot->setReservation($value);

        // THEN
        $response = $this->timeSlot->setReservation($value);

        self::assertInstanceOf(TimeSlot::class, $response);
        self::assertEquals($value, $this->timeSlot->GetReservation());
    }

    public function testWrongReservation(): void
    {
        // THEN
        $this->expectException(\Error::class);

        // GIVEN
        $value = "salut";

        // WHEN
        $this->timeSlot->addTimeSlot($value);

    }



}
