<?php

namespace App\Tests\Unit;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\TimeSlot;
use App\Entity\User;

class RoomTest extends ApiTestCase
{
    private Room $room;

    public function setUp(): void
    {
        parent::setUp();

        $this->room = new Room();
    }

    public function testGetName(): void
    {
        // GIVEN
        $value = 'galaxy';

        // WHEN
        $response = $this->room->setName($value);

        // THEN
        self::assertInstanceOf(Room::class, $response);
        self::assertEquals($value, $this->room->GetName());
    }
//
//    public function testWrongName(): void
//    {
//        // THEN
//        $this->expectException(\TypeError::class);
//
//        // GIVEN
//        $value = false;
//
//        // WHEN
//        $response = $this->room->setName($value);
//    }

    public function testDescription(): void
    {
        // GIVEN
        $value = 'galaxy description';

        // WHEN
        $response = $this->room->setDescription($value);

        // THEN
        self::assertInstanceOf(Room::class, $response);
        self::assertEquals($value, $this->room->GetDescription());
    }

    public function testMaxPlayers(): void
    {
        // GIVEN
        $value = 4;

        // WHEN
        $response = $this->room->setMaxPlayers($value);

        // THEN
        self::assertInstanceOf(Room::class, $response);
        self::assertEquals($value, $this->room->GetMaxPlayers());
    }


//    public function testWrongMaxPlayers(): void
//    {
//        // THEN
////        $this->expectException(\TypeError::class);
//
//        // GIVEN
//        $value = false;
//
//        // WHEN
//        $response = $this->room->setMaxPlayers($value);
//    }

    public function testPrice(): void
    {
        // GIVEN
        $value = 40;

        // WHEN
        $response = $this->room->setPrice($value);

        // THEN
        self::assertInstanceOf(Room::class, $response);
        self::assertEquals($value, $this->room->GetPrice());
    }

    public function testLocation(): void
    {
        // GIVEN
        $value = 'Paris';

        // WHEN
        $response = $this->room->setLocation($value);

        // THEN
        self::assertInstanceOf(Room::class, $response);
        self::assertEquals($value, $this->room->GetLocation());
    }

    public function testImgUrl(): void
    {
        // GIVEN
        $value = 'https://test.com/123';

        // WHEN
        $response = $this->room->setImgUrl($value);

        // THEN
        self::assertInstanceOf(Room::class, $response);
        self::assertEquals($value, $this->room->GetImgUrl());
    }

    public function testGetTimeSlot(): void
    {
        // GIVEN
        $value = new TimeSlot();
        $value1 = new TimeSlot();
        $value2 = new TimeSlot();

        // WHEN
        $this->room->addTimeSlot($value);
        $this->room->addTimeSlot($value1);
        $this->room->addTimeSlot($value2);

        // THEN
        self::assertCount(3, $this->room->getTimeSlots());
        self::assertTrue($this->room->getTimeSlots()->contains($value));
        self::assertTrue($this->room->getTimeSlots()->contains($value1));
        self::assertTrue($this->room->getTimeSlots()->contains($value2));

        $response = $this->room->removeTimeSlot($value);

        self::assertInstanceOf(Room::class, $response);
        self::assertCount(2, $this->room->getTimeSlots());
        self::assertFalse($this->room->getTimeSlots()->contains($value));
        self::assertTrue($this->room->getTimeSlots()->contains($value1));
        self::assertTrue($this->room->getTimeSlots()->contains($value2));

    }

    public function testWrongTimeSlot(): void
    {
        // THEN
        $this->expectException(\TypeError::class);

        // GIVEN
        $value = "salut";

        // WHEN
        $this->room->addTimeSlot($value);

    }

}
