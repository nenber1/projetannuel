<?php


namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\SendEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContactForm extends AbstractController
{

    /**
     * @param Request $request
     * @return JsonResponse
     */

    /**
     * @Route(
     *     name="contact",
     *     path="/contact",
     *     methods={"POST"}
     * )
     */
    public function __invoke(
        Request $request,
        SendEmail $sendEmail)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        try{
            $contentType = $request->getContentType();
            $content     = $request->getContent();
            if ($contentType != 'json' || !$content) {
                $response->setContent(json_encode(['fail' => 'empty content type or content type is not json format']));
                $response->setStatusCode(Response::HTTP_BAD_REQUEST,'Bad Request');
            }
            else
            {
                $data = json_decode($content, true);
                $firstname = $data['firstName'];
                $lastname = $data['lastName'];
                $email = $data['email'];
                $phone = $data['phone'];
                $message = $data['message'];
                $sendEmail->sendEmail([
                    'recipient_email' => "mpmathieu.pnr@gmail.com",
                    'subject'         => "Prise de contact",
                    'html_template'   => "email/contact.html.twig",
                    'context'         => [
                        'firstname' => $firstname,
                        'lastname'  => $lastname,
                        'phone'     => $phone,
                        'mil'     => $email,
                        'message'   => $message
                    ]
                ]);
                $response->setContent(json_encode(['success' => 'Votre message a été envoyé.']));
                $response->setStatusCode(Response::HTTP_OK,'Success');
            }
            return $response;
        }catch (Exception $e){
            $response->setContent(json_encode(['fail' => 'Internal Server Error']));
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR,'Bad Request');
            return $response;
        }
    }

}
