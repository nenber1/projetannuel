<?php


namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\SendEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ForgottenPasswordController extends AbstractController
{
    private UserPasswordHasherInterface $hasher;

    /**
     * @param Request $request
     * @return JsonResponse
     */



    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }
    /**
     * @Route(
     *     name="forgotten-password",
     *     path="/forgotten-password",
     *     methods={"POST"}
     * )
     */
    public function __invoke(
        UserRepository $repository,
        Request $request,
        SendEmail $sendEmail)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        try{
            $contentType = $request->getContentType();
            $content     = $request->getContent();
            if ($contentType != 'json' || !$content) {
                $response->setContent(json_encode(['fail' => 'empty content type or content type is not json format']));
                $response->setStatusCode(Response::HTTP_BAD_REQUEST,'Bad Request');
            }
            else
            {
                $data = json_decode($content, true);
                $email = $data['email'];
                $phone = $data['phone'];
                $birthday = $data['birthday'];
                $user =  $repository->findByEmailPhoneBirthday($email, $phone, $birthday);
                if($user and $user instanceof User) {
                    $random = substr(str_replace(['+', '/', '"', '=', '\\' ], '', base64_encode(random_bytes(12))), 0, 12);
                    $passHash = $this->hasher->hashPassword($user, $random);
                    $user->setPassword($passHash);
                    $em->persist($user);
                    $em->flush();
                    $sendEmail->sendEmail([
                        'recipient_email' => $email,
                        'subject'         => "Changement de mot de passe",
                        'html_template'   => "email/forgotten_password.html.twig",
                        'context'         => [
                            'newPassword' => $random
                        ]
                    ]);
                    $response->setContent(json_encode(['success' => 'Un email vous a été envoyé.']));
                    $response->setStatusCode(Response::HTTP_OK,'Success');
                }
                else{
                    $response->setContent(json_encode(['success' => 'Un email vous a été envoyé.']));
                    $response->setStatusCode(Response::HTTP_OK,'Success');
                }
            }
            return $response;
        }catch (Exception $e){
            $response->setContent(json_encode(['fail' => 'Internal Server Error']));
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR,'Bad Request');
            return $response;
        }
    }
    
}