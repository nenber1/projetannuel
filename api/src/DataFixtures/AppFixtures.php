<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Room;
use App\Entity\TimeSlot;
use App\Entity\Reservation;
use EasyRdf\Literal\Date;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Time;
use function Sodium\add;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHaserInterface
     */
    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function hashPassword(User $user, string $plainPassword): string
    {
        return $this->hasher->hashPassword($user, $plainPassword);
    }

    public function load(ObjectManager $manager): void
    {
        // Admin
        $userAdmin = new User();
        $userAdmin->setEmail('admin@admin.com');
        $hashedPassword = $this->hashPassword(
            $userAdmin,
            "Password$0"
        );
        $userAdmin->setPassword($hashedPassword);
        $userAdmin->setRoles(["ROLE_ADMIN"]);
        $userAdmin->setLastName('Administrateur');
        $userAdmin->setFirstName(' ');
        $userAdmin->setAdress('46 rue de Paris');
        $userAdmin->setCity('Paris');
        $userAdmin->setPhone('0102030405');
        $userAdmin->setBirthday(new Date());

        $manager->persist($userAdmin);

        $manager->flush();

        // Users
        $user1 = new User();
        $user1->setEmail('mathieu@pionnier.com');
        $hashedPassword = $this->hashPassword(
            $user1,
            "Password$0"
        );
        $user1->setPassword($hashedPassword);
        $user1->setRoles([]);
        $user1->setLastName('Pionnier');
        $user1->setFirstName('Mathieu');
        $user1->setAdress('46 rue de Paris');
        $user1->setCity('Paris');
        $user1->setPhone('0102030405');
        $user1->setBirthday(new Date());

        $manager->persist($user1);

        $manager->flush();

        $user2 = new User();
        $user2->setEmail('thomas@pionnier.com');
        $hashedPassword = $this->hashPassword(
            $user2,
            "Password$0"
        );
        $user2->setPassword($hashedPassword);
        $user2->setRoles([]);
        $user2->setLastName('Pionnier');
        $user2->setFirstName('Mathieu');
        $user2->setAdress('46 rue de Paris');
        $user2->setCity('Paris');
        $user2->setPhone('0102030405');
        $user2->setBirthday(new Date());

        $manager->persist($user2);

        $manager->flush();

        // Rooms
        $room1 = new Room();
        $room1->setName('Galaxy');
        $room1->setMaxPlayers(4);
        $room1->setPrice(20);
        $room1->setDescription('Venez voyager sur la Galaxy !');
        $room1->setLocation('Paris');

        $manager->persist($room1);

        $manager->flush();

        $room2 = new Room();
        $room2->setName('Mars');
        $room2->setMaxPlayers(3);
        $room2->setPrice(30);
        $room2->setDescription('Venez voyager sur Mars !');
        $room2->setLocation('Paris');

        $manager->persist($room2);

        $manager->flush();

        $room3 = new Room();
        $room3->setName('Venus');
        $room3->setMaxPlayers(2);
        $room3->setPrice(25);
        $room3->setDescription('Venez voyager sur Venus !');
        $room3->setLocation('Paris');

        $manager->persist($room3);

        $manager->flush();

        // Timeslot
        $timeslot1 = new TimeSlot();
        $timeslot1->setAvailable(true);
        $timeslot1->setRoomId($room1);
        $timeslot1->setSlotStart(new \DateTime());
        $endDate = new \DateTime();
        $interval = new \DateInterval('PT1H');
        $timeslot1->setSlotEnd($endDate->add($interval));


        $manager->persist($timeslot1);

        $manager->flush();

        $timeslot2 = new TimeSlot();
        $timeslot2->setAvailable(true);
        $timeslot2->setRoomId($room2);
        $timeslot2->setSlotStart(new \DateTime());
        $endDate = new \DateTime();
        $interval = new \DateInterval('PT1H');
        $timeslot2->setSlotEnd($endDate->add($interval));


        $manager->persist($timeslot2);

        $manager->flush();

        $timeslot3 = new TimeSlot();
        $timeslot3->setAvailable(true);
        $timeslot3->setRoomId($room3);
        $timeslot3->setSlotStart(new \DateTime());
        $endDate = new \DateTime();
        $interval = new \DateInterval('PT1H');
        $timeslot3->setSlotEnd($endDate->add($interval));


        $manager->persist($timeslot3);

        $manager->flush();

        // Reservation
        $reservation = new Reservation();
        $reservation->setRoomId($room1);
        $reservation->setClientId($user1);
        $reservation->setCreatedAt(new \DateTime());
        $reservation->setQuantity(2);
        $reservation->setSlotId($timeslot1);
        $reservation->setStatus(true);

        $manager->persist($reservation);

        $manager->flush();

        $reservation = new Reservation();
        $reservation->setRoomId($room2);
        $reservation->setClientId($user2);
        $reservation->setCreatedAt(new \DateTime());
        $reservation->setQuantity(3);
        $reservation->setSlotId($timeslot2);
        $reservation->setStatus(true);

        $manager->persist($reservation);

        $manager->flush();

    }
}
