<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TimeSlotRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TimeSlotRepository::class)
 */
#[ApiResource]
class TimeSlot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $available;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $slotStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $slotEnd;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="timeSlots")
     */
    private $roomId;

    /**
     * @ORM\OneToOne(targetEntity=Reservation::class, mappedBy="slotId", cascade={"persist", "remove"})
     */
    private $reservation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(?bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getSlotStart(): ?\DateTimeInterface
    {
        return $this->slotStart;
    }

    public function setSlotStart(?\DateTimeInterface $slotStart): self
    {
        $this->slotStart = $slotStart;

        return $this;
    }

    public function getSlotEnd(): ?\DateTimeInterface
    {
        return $this->slotEnd;
    }

    public function setSlotEnd(?\DateTimeInterface $slotEnd): self
    {
        $this->slotEnd = $slotEnd;

        return $this;
    }

    public function getRoomId(): ?Room
    {
        return $this->roomId;
    }

    public function setRoomId(?Room $roomId): self
    {
        $this->roomId = $roomId;

        return $this;
    }

    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): self
    {
        // unset the owning side of the relation if necessary
        if ($reservation === null && $this->reservation !== null) {
            $this->reservation->setSlotId(null);
        }

        // set the owning side of the relation if necessary
        if ($reservation !== null && $reservation->getSlotId() !== $this) {
            $reservation->setSlotId($this);
        }

        $this->reservation = $reservation;

        return $this;
    }
}
