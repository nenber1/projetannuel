<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"user_read"}}
 *          },
 *          "post",
 *          "me"={

 *               "pagination_enabled"="false",
 *               "path"="/me",
 *               "method"="get",
 *               "controller"=MeController::class,
 *               "read":"false"
 *          },
 *          "forgotten_password"={
 *              "method"="post",
 *              "path"="/forgotten-password",
 *              "controller"=App\Controller\ForgottenPasswordController::class,
 *              "denormalization_context"={"groups"={"forgottenPassword"}}
 *          },
 *          "contact"={
 *              "method"="post",
 *              "path"="/contact",
 *              "controller"=App\Controller\ContactForm::class,
 *              "denormalization_context"={"groups"={"contact"}}
 *          },
 *          "create_user"={
 *              "method"="POST",
 *              "path"="/users/create",
 *              "controller"=App\Controller\CreateUser::class
 *          }
 *      },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"user_details_read"}}

 *          },
 *          "patch",
 *          "update_user"={
 *              "method"="PUT",
 *              "path"="/users/{id}/update",
 *              "controller"=App\Controller\UpdateUser::class,
 *              "normalization_context"={"groups"={"user_update"}}
 *          },
 *          "update_password"={
 *              "method"="PUT",
 *              "path"="users/{id}/update-password",
 *              "controller"=App\Controller\UserChangePassword::class,
 *              "normalization_context"={"groups"={"user:passwordUpdate"}}
 *          },
 *          "put",
 *          "delete"
 *     }
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user_read", "user_details_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user_read", "user_details_read", "forgottenPassword", "contact"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"passwordUpdate"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user_read", "user_details_read", "user_update", "contact"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user_read", "user_details_read", "user_update", "contact"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read", "user_details_read", "user_update"})
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read", "user_details_read", "user_update"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_read", "user_details_read", "user_update", "forgottenPassword", "contact"})
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="clientId")
     */
    private $reservations;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user_read", "user_details_read", "user_update", "forgottenPassword"})
     */
    private $birthday;


    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        // $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setClientId($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getClientId() === $this) {
                $reservation->setClientId(null);
            }
        }

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->id;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }
}
