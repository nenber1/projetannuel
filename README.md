**/!\ Si erreur .env au build/up, commenter le fichier**

Remove all docker containers and volumes:

> docker container stop $(docker container ls -aq)
> docker system prune -af --volumes

Go to the root folder, then build the docker images and create the containers

> docker-compose build --pull
> docker-compose up

Database url:

> DATABASE_URL= postgresql://postgres:root@database:5432/api?serverVersion=13

Init api platform, go to api folder

> docker-compose exec php bin/console d:d:c
> docker-compose exec php bin/console make:entity
> docker-compose exec php bin/console d:s:u --force
> docker-compose exec make:migration
> docker-compose exec migration:migrate
> -----clear cache if api plateform doesn't keep up with entities
> docker-compose exec php bin/console clear:cache

Add a bundle for symfony

> docker-compose php composer require <BUNDLE_NAME>

Adresse for api platform documentations:

> https://localhost/docs

Adress for front vuejs:

> https://localhost:3001/

Adress for kibana:

> http://localhost:5601/app/home

For logs with filebeat for login, filters are :

> container.image.name: projetannuel_caddy; json.request.uri: /login; json.request.headers.Access-Control-Request-Method: POST

Run tests with jest:

> npm run test:unit

Run tests with phpunit:

> php bin/phpunit
