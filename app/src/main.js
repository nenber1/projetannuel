import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueRouter from "vue-router";
import Router from "./routes";
import "./assets/_global.scss";
import Calendar from "v-calendar/lib/components/calendar.umd";
import DatePicker from "v-calendar/lib/components/date-picker.umd";
import Datepicker from "vuejs-datepicker";
import Vuelidate from "vuelidate";
import "./axios";
import './registerServiceWorker'
import VCurrencyField from 'v-currency-field'
import DatetimePicker from 'vuetify-datetime-picker'



// Register components in your 'main.js'
Vue.component("calendar", Calendar);
Vue.use(Vuelidate);
Vue.component("date-picker", DatePicker);
Vue.component("datepicker", Datepicker);

Vue.use(VueRouter);
Vue.use(VCurrencyField, {
  locale: 'pt-BR',
  decimalLength: 2,
  autoDecimalMode: true,
  min: null,
  max: null,
  defaultValue: 0,
  valueAsInteger: false,
  allowNegative: true
})
Vue.use(DatetimePicker)


const router = new VueRouter({ routes: Router, mode: "history" });

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: (h) => h(App),
  router: router,
}).$mount("#app");

export default {
  components: {
    Calendar,
    DatePicker,
    Datepicker,
  },
};
