import HomePage from "./components/HomePage.vue";
import Summary from "./components/Summary.vue";
import Rooms from "./components/room/Rooms.vue";
import Room from "./components/room/Room.vue";
import Map from "./components/Map.vue";
import Reservation from "./components/Reservation.vue";
import VuemikTest from "./components/formik/VuemikTest.vue";
import Page404 from "./components/Page404.vue";
import Register from "./components/User/Register.vue";
import ForgottenPassword from "./components/User/ForgottenPassword.vue";
import Profil from "./components/User/Profil.vue";
import Contact from "./components/Contact.vue";
import MyReservations from "./components/User/Reservations.vue";
import EditReservation from "./components/User/EditReservation.vue";
import UpdatePassword from "./components/User/UpdatePassword.vue";
import Dashboard from "./components/Admin/Dashboard.vue";
import Success from "./components/Payment/Success.vue";
import Failed from "./components/Payment/Failed.vue"

export default [
  { name: "HomePage", path: "/", component: HomePage },
  { name: "Dashboard", path: "/admin/dashboard", component: Dashboard },

  { name: "Contact", path: "/contact", component: Contact },
  { name: "Summary", path: "/summary", component: Summary },
  { name: "Register", path: "/register", component: Register },
  {
    name: "ForgottenPassword",
    path: "/forgotten-password",
    component: ForgottenPassword,
  },
  { name: "Rooms", path: "/rooms", component: Rooms },
  { name: "Room", path: "/room/:id", component: Room, props: true },
  { name: "Map", path: "/map", component: Map },
  { name: "Profil", path: "/profil", component: Profil },
  { name: "Reservations", path: "/reservations", component: MyReservations },
  { name: "UpdatePassword", path: "/mon-compte", component: UpdatePassword },
  {
    name: "Reservation",
    path: "/reservation/room/:id",
    component: Reservation,
    props: true,
  },
  {
    name: "EditReservation",
    path: "/reservation/:id",
    component: EditReservation,
    props: true,
  },
  {
    name: "Success",
    path: "/payment/success",
    component: Success,
  },
  {
    name: "Failed",
    path: "/payment/failed",
    component: Failed,
  },
  // { name: 'ReservationEdit',
  //   path: "/admin/reservations/:id/edit",
  //   component: ReservationEdit,
  //   props: true,
  // },0102030405
  { name: "Vuemik", path: "/vuemik", component: VuemikTest },
  { name: "Page404", path: "/404", alias: "*", component: Page404 },
  { path: "/:pathMatch(.*)*", alias: "/404", component: Page404 },
];
