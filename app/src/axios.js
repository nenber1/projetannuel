import axios from "axios";

axios.defaults.baseURL = "https://35.187.15.66/";
axios.defaults.headers.patch['Content-Type'] = 'application/merge-patch+json';
axios.defaults.headers['Content-Type'] = 'application/json';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.put['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json';
// axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.delete['Content-Type'] = 'application/json';
axios.defaults.headers.common["Authorization"] = "Bearer " + localStorage.getItem("token");
