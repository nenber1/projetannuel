import { mount } from "@vue/test-utils";
import Reservation from "@/components/Admin/Datatable/Reservations.vue";

describe("Reservation.vue", () => {
  it("basic vue instance", () => {
    //GIVEN
    const wrapper = mount(Reservation, {
      propsData: {
        Reservation: {
          name: "Reservation",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });
});
