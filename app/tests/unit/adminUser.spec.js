import { mount } from "@vue/test-utils";
import User from "@/components/Admin/Datatable/Users.vue";

describe("User.vue", () => {
  it("basic vue instance", () => {
    //GIVEN
    const wrapper = mount(User, {
      propsData: {
        User: {
          name: "User",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });
});
