import { mount } from "@vue/test-utils";
import Timeslot from "@/components/Admin/Datatable/TimeSlots.vue";

describe("Timeslot.vue", () => {
  it("basic vue instance", () => {
    //GIVEN
    const wrapper = mount(Timeslot, {
      propsData: {
        Timeslot: {
          name: "Timeslot",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });
});
