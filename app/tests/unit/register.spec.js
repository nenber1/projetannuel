import { mount, shallowMount } from "@vue/test-utils";
import Register from "@/components/User/Register.vue";

describe("Register.vue", () => {
  it("it's a vue instance", () => {
    //GIVEN
    const wrapper = mount(Register, {
      propsData: {
        register: {
          name: "Register",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });

  it("it's should not display error message when user complete correctly form", () => {
    // GIVEN
    const wrapper = shallowMount(Register, {
      data() {
        return {
          firstName: "Yolanda",
          lastName: "Toto",
          email: "toto@live.fr",
          password: "azerty",
          birthday: "23/09/1995",
          phone: "010202394",
          adress: "33 rue des bruns",
          city: "Reims",
        };
      },
    });

    //THEN
    // met à jour `form` et vérifie que `error` n'est plus restituée
    wrapper.setData({
      firstName: "Yolanda",
      lastName: "Toto",
      email: "toto@live.fr",
      password: "azerty",
      birthday: "23/09/1995",
      phone: "010202394",
      adress: "33 rue des bruns",
      city: "Reims",
    });
    expect(wrapper.find(".resultBool").exists()).toBeFalsy();
  });

  it("it's should call register function when submit button is clicked", () => {
    //GIVEN
    const wrapper = mount(Register, {
      propsData: {
        firstName: "Yolanda",
        lastName: "Toto",
        email: "toto@live.fr",
        password: "azerty",
        birthday: "23/09/1995",
        phone: "010202394",
        adress: "33 rue des bruns",
        city: "Reims",
      },
    });

    const handleSubmit = jest.fn();

    //WHEN
    wrapper.setMethods({
      handleSubmit: handleSubmit,
    });

    wrapper.find("button").trigger("click");
    //THEN
    expect(handleSubmit).toHaveBeenCalled;
  });
});
