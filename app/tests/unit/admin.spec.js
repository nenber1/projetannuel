import { mount } from "@vue/test-utils";
import Edit from "@/components/Admin/Modal/Edit.vue";

describe("Edit.vue", () => {
  it("it's a vue instance", () => {
    //GIVEN
    const wrapper = mount(Edit, {
      propsData: {
        Edit: {
          name: "Edit",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });

  //   it("it's should edit email", () => {
  //     // GIVEN
  //     const wrapper = mount(Edit, {
  //       propsData: {
  //         Edit: {
  //           name: "Edit",
  //           completed: false,
  //         },
  //       },
  //     });

  //     //THEN

  //     let element_email = wrapper.find("#emailTest");
  //     element_email.setValue("test@gmail.com");

  //     expect(wrapper.vm.editedItem).toBe(true);
  //   });
});
