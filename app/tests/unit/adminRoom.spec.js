import { mount } from "@vue/test-utils";
import Room from "@/components/Admin/Datatable/Rooms.vue";

describe("Room.vue", () => {
  it("basic vue instance", () => {
    //GIVEN
    const wrapper = mount(Room, {
      propsData: {
        Room: {
          name: "Room",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });
});
