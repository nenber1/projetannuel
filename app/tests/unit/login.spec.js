import { mount, shallowMount } from "@vue/test-utils";
import App from "@/components/modal/LoginModal.vue";
import Register from "@/components/User/Register";

describe("App.vue", () => {
  it("it's a vue instance", () => {
    //GIVEN
    const wrapper = mount(App, {
      mocks: {
        $vuetify: { breakpoint: {} },
      },
      propsData: {
        app: {
          name: "App",
          completed: false,
        },
      },
    });
    //WHEN
    //THEN
    expect(wrapper.isVueInstance);
  });

  it("it's should not display error message when user complete correctly form", () => {
    // GIVEN
    const wrapper = shallowMount(Register, {
      data() {
        return {
          email: "toto@live.fr",
          password: "azerty",
        };
      },
    });

    //THEN
    // met à jour `form` et vérifie que `error` n'est plus restituée
    wrapper.setData({
      email: "toto@live.fr",
      password: "azerty",
    });
    expect(wrapper.find(".resultBool").exists()).toBeFalsy();
  });

  it("it's should call register function when submit button is clicked", () => {
    //GIVEN
    const wrapper = mount(Register, {
      propsData: {
        email: "toto@live.fr",
        password: "azerty",
      },
    });

    const handleSubmit = jest.fn();

    //WHEN
    wrapper.setMethods({
      handleSubmit: handleSubmit,
    });

    wrapper.find("button").trigger("click");
    //THEN
    expect(handleSubmit).toHaveBeenCalled;
  });
});
